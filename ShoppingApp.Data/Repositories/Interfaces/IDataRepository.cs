using System;
using System.Collections.Generic;
using ShoppingApp.Models.Models.Interfaces;


namespace ShoppingApp.Data.Repositories.Interfaces
{
    public interface IDataRepository<T> where T : IEntity
    {
       T GetById(int id);

       T GetByCode(string code);

       List<T> GetAll();
    }
}