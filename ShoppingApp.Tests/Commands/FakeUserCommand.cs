using System;
using ShoppingApp.Console.Commands.Interfaces;

namespace ShoppingApp.Tests
{
    public class FakeUserCommand : ICustomCommand
    {
        protected FakeUserCommand()
        {}

        public FakeUserCommand(string text)
        {
            Data = text;
        }

        public object Data {get; set;}

        public object GetResult()
        {
            return Data;
        }

        public override string ToString()
        {
            return Data.ToString();
        }
    }
}