using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShoppingApp.Tests.ModelBuilders;
using ShoppingApp.Models.Models;
using System.Collections.Generic;
using System.Linq;
using ShoppingApp.Console.Parsers;
using System.Diagnostics;
using ShoppingApp.Console.Commands.Interfaces;
using ShoppingApp.Console;

namespace ShoppingApp.Tests
{
    [TestClass]
    public class Scenario3Test
    {
        [TestMethod]
        public void Items_Can_OnSpecial_nAmount_off_normal_price()
        {
            var shop = TestShopBuilder.Instance.BuildEntity();

            var item = shop.Items.First();

            var quantity = 1;

            var cartItem = new CartItem(itemId: item.Id, item: item, quantity: quantity);

            cartItem.ApplyFlatDiscount(5);

            var result = (cartItem.TotalCostAmount != (item.Price * quantity));

            System.Console.WriteLine(cartItem.TotalCostAmount + " vs "+ item.Price * quantity);
            
            Assert.IsTrue(result,"Percent Discount not applied");
        }

         [TestMethod]
        public void Program_Should_Apply_Flat_Discount()
        {
            var shop = TestShopBuilder.Instance.BuildEntity();

            var commandList = new List<ICustomCommand>();

            commandList.Add(new FakeUserCommand("1:Apple"));
            commandList.Add(new FakeUserCommand("2:Bread"));
            commandList.Add(new FakeUserCommand("3:Fryer:10"));

            var parser = new UserInputParser(shop);

            var cartItems = parser.Parse(commandList);

            var result = cartItems.Any(x=>x.DiscountType == DiscountType.Flat);

            Assert.IsTrue(result, "No Percentage discount applied.");
        }
    }    
}