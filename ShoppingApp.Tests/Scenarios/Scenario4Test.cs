using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShoppingApp.Tests.ModelBuilders;
using ShoppingApp.Models.Models;
using System.Collections.Generic;
using System.Linq;
using ShoppingApp.Console.Parsers;
using System.Diagnostics;
using ShoppingApp.Console.Commands.Interfaces;
using ShoppingApp.Console;

namespace ShoppingApp.Tests
{
    [TestClass]
    public class Scenario4Test
    {
        [TestMethod]
        public void Items_Can_Apply_Highest_Discount_Amount()
        {
            var shop = TestShopBuilder.Instance.BuildEntity();

            var item = shop.Items.Last();

            var quantity = 1;

            var cartItem = new CartItem(itemId: item.Id, item: item, quantity: quantity);

            cartItem.ApplyFlatDiscount(5);

            cartItem.ApplyPercentDiscount(90);

            var result = (cartItem.TotalCostAmount == 10);

            Assert.IsTrue(result,"Highest discount not applied.");
        }     
    }    
}