using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShoppingApp.Tests.ModelBuilders;
using ShoppingApp.Models.Models;
using System.Collections.Generic;
using System.Linq;
using ShoppingApp.Console.Parsers;
using System.Diagnostics;
using ShoppingApp.Console.Commands.Interfaces;
using ShoppingApp.Console;

namespace ShoppingApp.Tests
{
    [TestClass]
    public class Scenario1Test
    {
        [TestMethod]
        public void Shop_Should_Contain_At_Least_10_Types()
        {
            var shop = TestShopBuilder.Instance.BuildEntity();

            var keyCount = shop.Items
                .GroupBy(x => x.ItemType)
                .Select(x => x.Key)
                .Count();

            var result = keyCount >= 10;

            Assert.IsTrue(result, "Shop must contain at least 10 Item Types.");
        }

        [TestMethod]
        public void User_Must_Be_Able_To_Enter_N_Number_Of_Items()
        {
            var shop = TestShopBuilder.Instance.BuildEntity();

            List<CartItem> cartItems = new List<CartItem>();

            var commandList = new List<ICustomCommand>();

            commandList.Add(new FakeUserCommand("1:Apple"));
            commandList.Add(new FakeUserCommand("2:Bread"));
            commandList.Add(new FakeUserCommand("3:Fryer"));

            var parser = new UserInputParser(shop);

            cartItems = parser.Parse(commandList);

            var result = cartItems.Count() != 0;

            Assert.IsTrue(result, "No Items detected.");
        }

        [TestMethod]
        public void Program_Should_Return_Total_Cost()
        {
            var shop = TestShopBuilder.Instance.BuildEntity();

            var commandList = new List<ICustomCommand>();

            commandList.Add(new FakeUserCommand("1:Apple"));
            commandList.Add(new FakeUserCommand("2:Bread"));
            commandList.Add(new FakeUserCommand("3:Fryer"));

            var result = Program.Instance.ProcessUserShoppingInput(commandList);

            Assert.IsNotNull(result, "Program cannot return total cost.");

            System.Console.WriteLine("Total Cost: " + result);
        }
    }
}
