using Microsoft.VisualStudio.TestTools.UnitTesting;
using ShoppingApp.Models.Models;
using System.Collections.Generic;
using System.Linq;
using ShoppingApp.Data.Repositories.Interfaces;
using Moq;
using ShoppingApp.Tests.ModelBuilders;

namespace ShoppingApp.Tests
{
    [TestClass]
    public class ShopTest
    {
        
        [TestMethod]
        public void Shop_Initialize()
        {
            var result = TestShopBuilder.Instance.BuildEntity();
            
            Assert.IsNotNull(result,"Object is null.");
        }      

        [TestMethod]
        public void Shop_Should_Have_Items()
        {
            var shop = TestShopBuilder.Instance.BuildEntity();

            var result = shop.Items.Any();

            Assert.IsTrue(result,"Items is empty.");
        }      
    }
}
