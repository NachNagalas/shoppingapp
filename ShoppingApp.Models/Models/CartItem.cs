using System;
using System.Collections.Generic;
using ShoppingApp.Models.Models;
using ShoppingApp.Models.Models.Abstracts;

public class CartItem : BaseEntity
{
    protected CartItem()
    {
    }

    public CartItem(int itemId,
    Item item,
    int quantity)
    {
        ItemId = itemId;
        Item = item;
        Quantity = quantity;
    }

    public int ItemId { get; protected set; }

    public Item Item { get; protected set; }

    public int Quantity { get; protected set; }

    public DiscountType DiscountType { get; protected set; }

    public double DiscountAmount { get; protected set; }

    public double TotalCostAmount
    {
        get
        {
            if (DiscountType != DiscountType.None) return Quantity * (Item.Price - DiscountAmount);
            else return Quantity * Item.Price;
        }
    }

    public void ApplyPercentDiscount(double percentage)
    {
        DiscountType = DiscountType.Percentage;
        DiscountAmount = Item.Price * (percentage / 100);
    }

    public void ApplyFlatDiscount(double flatAmount)
    {
        DiscountType = DiscountType.Flat;
        DiscountAmount = flatAmount;
    }
}