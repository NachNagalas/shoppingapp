using System;
using System.Collections.Generic;
using System.Linq;
using ShoppingApp.Models.Models.Abstracts;

public class Cart : BaseEntity
{
    protected Cart()
    {
    }

    public Cart(List<CartItem> cartItems)
    {
        CartItems = cartItems;
    }

    public List<CartItem> CartItems { get; protected set; } = new List<CartItem>();

    public double TotalCost
    {
        get
        {
            return CartItems.Sum(x => (x.TotalCostAmount));
        }
    }
}