using System;
using ShoppingApp.Models.Models.Abstracts;

namespace ShoppingApp.Models.Models
{
    public class Item : BaseEntity
    {
        protected Item()
        {}

        public Item(
            string name,
            string alias,
            string code,
            double price,
            ItemType itemType
        )
        {
            Name = name;
            Alias = alias;
            Code = code;
            Price = price;
            ItemType = itemType;
        }

        public string Name {get; protected set;}

        public string Alias {get; protected set;}

        public string Code {get; protected set;}

        public double Price {get; protected set;}

        public ItemType ItemType {get; protected set;}
    }
}