using System;
using ShoppingApp.Models.Models.Interfaces;

namespace ShoppingApp.Tests.ModelBuilders.Interfaces
{
    public interface IBuilder<T> where T: IEntity
    {
        T BuildEntity();
    }
}