using System;

namespace ShoppingApp.Models.Models.Interfaces
{
    public interface IEntity
    {
        int Id {get; set;}
    }
}