using System;
using System.Collections.Generic;
using ShoppingApp.Models.Models.Abstracts;

namespace ShoppingApp.Models.Models
{
    public class Shop : BaseEntity
    {
        protected Shop()
        {}

         public Shop(
             string name,
             string alias,
             string code,
             List<Item> items
         )
        {
            Name = name;
            Alias = alias;
            Code = code;
            Items = items;
        }

        

        public string Name {get; protected set;}

        public string Alias {get; protected set;}
        
        public string Code {get; protected set;}

        public List<Item> Items {get; protected set;} = new List<Item>();
    }
}