using System;
using ShoppingApp.Models.Models.Interfaces;

namespace ShoppingApp.Models.Models.Abstracts
{
    public abstract class BaseEntity : IEntity
    {
        public int Id { get; set; }
    }
    
}