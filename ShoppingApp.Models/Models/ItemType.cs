using System;
using ShoppingApp.Models.Models.Abstracts;

namespace ShoppingApp.Models.Models
{
    public class ItemType : BaseEntity
    {
        protected ItemType()
        {}

        public ItemType(
            string name,
            string alias,
            string code
        )
        {
            Name = name;
            Alias = alias;
            Code = code;
        }

        public string Name {get; protected set;}

        public string Alias {get; protected set;}

        public string Code {get; protected set;}
    }
}