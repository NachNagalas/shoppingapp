using System;

public enum DiscountType : byte
{
    None=0,
    Percentage=1,
    Flat=2
}