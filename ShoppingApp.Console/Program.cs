﻿using System;
using System.Collections.Generic;
using ShoppingApp.Console.Commands.Interfaces;
using ShoppingApp.Console.Parsers;
using ShoppingApp.Models.Models;
using ShoppingApp.Console.ModelBuilders;

namespace ShoppingApp.Console
{
    public class Program
    {
        public static Program Instance { get; } = new Program();
        
        public static void Main(string[] args)
        {

        }

        public double ProcessUserShoppingInput(List<ICustomCommand> commandList)
        {
            List<CartItem> cartItems = new List<CartItem>();

            var shop = ShopBuilder.Instance.BuildEntity();

            var parser = new UserInputParser(shop);

            cartItems = parser.Parse(commandList);

            var cart = new Cart(cartItems);

            return cart.TotalCost;
        }
    }
}
