using System;

namespace ShoppingApp.Console.Commands.Interfaces
{
    public interface ICustomCommand
    {    
        object Data {get;set;}

        object GetResult();    

        string ToString();
    }
    
}