using System;
using ShoppingApp.Console.Commands.Interfaces;

namespace ShoppingApp.Console.Commands
{
    public class UserCommand : ICustomCommand
    {
        public UserCommand(string input)
        {
            UserInput = input;
        }

        public string UserInput {get;protected set;}

        public object Data {get; set;}

        public object GetResult()
        {
            return Data;
        }
    }
}