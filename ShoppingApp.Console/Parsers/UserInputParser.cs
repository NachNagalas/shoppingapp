using System;
using System.Linq;
using System.Collections.Generic;
using ShoppingApp.Console.Commands;
using ShoppingApp.Console.Commands.Interfaces;
using ShoppingApp.Models.Models;

namespace ShoppingApp.Console.Parsers
{
    public class UserInputParser
    {
        public UserInputParser(Shop shop)
        {
            Shop = shop;
        }

        public Shop Shop { get; }

        public CartItem Parse(ICustomCommand command)
        {
            CartItem cartItem = null;

            var stringArray = command.ToString().Split(":");
            if (stringArray[0].All(Char.IsDigit))
            {
                cartItem = GetCartItem(command.ToString());
            }
            else
            {
                string commandHeader = stringArray[0];
                if(commandHeader == "ALL")
                {
                
                }
                else
                {
                    var itemTypeCode = Shop.Items
                    .SingleOrDefault(x => x.ItemType.Code == commandHeader);

                if (itemTypeCode != null)
                {
                    
                }
                }
            }
            
            return cartItem;
        }

        private CartItem GetCartItem(string command)
        {
            var stringArray = command.Split(":");

            var count = stringArray.Count();

            if (count < 1) return null;

            var itemQuantity = stringArray[0];
            var itemCode = stringArray[1];

            var item = Shop.Items
            .SingleOrDefault(x => x.Code == itemCode);

            if (item == null) throw new Exception("Item does not exist");

            var quantity = Convert.ToInt32(itemQuantity);

            var cartItem = new CartItem(item.Id, item, quantity);

            if (count >= 3)
            {
                var itemDiscount = stringArray[2];
                var numericString = new String(itemDiscount.Where(Char.IsDigit).ToArray());

                if (itemDiscount.Any(x => x == '%'))
                {
                    var discountPercent = Convert.ToDouble(numericString);
                    cartItem.ApplyPercentDiscount(discountPercent);
                }
                else
                {
                    var discountNumeric = Convert.ToDouble(numericString);
                    cartItem.ApplyFlatDiscount(discountNumeric);
                }
            }

            return cartItem;
        }
    }
}