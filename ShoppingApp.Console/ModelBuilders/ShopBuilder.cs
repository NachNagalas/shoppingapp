using System;
using ShoppingApp.Models.Models;
using System.Collections.Generic;
using System.Linq;
using ShoppingApp.Data.Repositories.Interfaces;
using Moq;
using ShoppingApp.Tests.ModelBuilders.Interfaces;

namespace ShoppingApp.Console.ModelBuilders
{
    public class ShopBuilder : IBuilder<Shop>
    {
        protected ShopBuilder()
        {
        }

        public static ShopBuilder Instance { get; } = new ShopBuilder();

        public Shop BuildEntity()
        {
            var itemTypeMock = new Mock<IDataRepository<ItemType>>();

            itemTypeMock.Setup(x => x.GetAll()).Returns(
                new List<ItemType>
                {
                    new ItemType(name: "Fruit",alias: "Fruit",code: "Fruit"),
                    new ItemType(name: "Bread",alias: "Bread",code: "Bread"),
                    new ItemType(name: "Vegetable",alias: "Vegetable",code: "Vegetable"),
                    new ItemType(name: "Tools",alias: "Tools",code: "Tools"),
                    new ItemType(name: "Appliances",alias: "Appliances",code: "Appliances"),
                    new ItemType(name: "Utensils",alias: "Utensils",code: "Utensils"),
                    new ItemType(name: "Instruments",alias: "Instruments",code: "Instruments"),
                    new ItemType(name: "Paper",alias: "Paper",code: "Paper"),
                    new ItemType(name: "Writing",alias: "Writing",code: "Writing"),
                    new ItemType(name: "Ingredients",alias: "Ingredients",code: "Ingredients")
                }
            );

            var itemTypes = (itemTypeMock.Object as IDataRepository<ItemType>).GetAll();

            var itemMock = new Mock<IDataRepository<Item>>();

            itemMock.Setup(x => x.GetAll()).Returns(
              new List<Item>
              {
                  new Item(name:"Apple",alias:"Apple",code:"Apple",price:10,itemType: itemTypes.Single(x=>x.Code == "Fruit")),
                  new Item(name:"Bread",alias:"Bread",code:"Bread",price:20,itemType: itemTypes.Single(x=>x.Code == "Bread")),
                  new Item(name:"Carrot",alias:"Carrot",code:"Carrot",price:30,itemType: itemTypes.Single(x=>x.Code == "Vegetable")),
                  new Item(name:"Drum",alias:"Drum",code:"Drum",price:40,itemType: itemTypes.Single(x=>x.Code == "Tools")),
                  new Item(name:"Electric Fan",alias:"Electric Fan",code:"Electric Fan",price:50,itemType: itemTypes.Single(x=>x.Code == "Appliances")),
                  new Item(name:"Fryer",alias:"Fryer",code:"Fryer",price:60,itemType: itemTypes.Single(x=>x.Code == "Utensils")),
                  new Item(name:"Guitar",alias:"Guitar",code:"Guitar",price:70,itemType: itemTypes.Single(x=>x.Code == "Instruments")),
                  new Item(name:"Handbook",alias:"Handbook",code:"Handbook",price:80,itemType: itemTypes.Single(x=>x.Code == "Paper")),
                  new Item(name:"Ink",alias:"Ink",code:"Ink",price:90,itemType: itemTypes.Single(x=>x.Code == "Writing")),
                  new Item(name:"Jelly",alias:"Jelly",code:"Jelly",price:100,itemType: itemTypes.Single(x=>x.Code == "Ingredients")),
              }
            );

            var items = (itemMock.Object as IDataRepository<Item>).GetAll();

            var shopMock = new Mock<IDataRepository<Shop>>();

            shopMock.Setup(x => x.GetById(1)).Returns(
                new Shop(
                name: "HelloWorld",
                alias: "HelloWorld",
                code: "HelloWorld",
                items: items));

            var shop = (shopMock.Object as IDataRepository<Shop>).GetById(1);

            return shop;
        }
    }
}